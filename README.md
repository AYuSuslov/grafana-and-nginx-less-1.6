## Описание задания

Напишите Докер компоСТ (docker-compose.yml со сл условиями:
1. 3 контейнера nginx1 nginx2 grafana (https://hub.docker.com/r/grafana/grafana)
2 Nginx1 port 8001 на Grafana
2 Nginx2 port 8002 на Grafana
3 Grafana не пробрасывать ничего наружу
Она не должна подниматься если вы её стопорите руками.
4 Nginx1 и Nginx2 в разных сетях.
5 И конечно 2 конфига нжинкса которые будут пробрасывать на Grafana (пример конфиг с лес1.1 джанго конф)
Ответ в Гитлаб

Задача со*
Сделать хелсчек Nginx1 в контейнере с Grafana
Уронить Nginx1 посмотреть что будет) Скрины в ответ на задачу. Либо пастбин.

## Ответ

Ссылки на работающие приложения *Grafana* на экземпляре VM Debian GCP: http://35.234.123.52:8001/ и http://35.234.123.52:8002/ 
Ссылка на репозиторий Gitlab (docker-compose.yml файл и два конфига nginx1.conf и nginx2.conf): https://gitlab.com/AYuSuslov/grafana-and-nginx-less-1.6

Для задания со \* healthckeck в контейнере с Grafana выполнен командой *"curl -f http://nginx1:8001"*. Работа проверена остановкой контейнера с Nginx1, состояние контейнера с Grafana переходит в *"unhealthy"*, после запуска Nginx1 переходит обратно в *"healthy"*.
